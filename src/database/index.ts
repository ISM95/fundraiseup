import { Collection, Db, MongoClient } from 'mongodb'
import {Customer} from './models/customer'

interface Collections {
	customers: Collection<Customer>
}

export class MongoDatabase {
	public mongoClient = new MongoClient(process.env.DB_URI)
	public collections: Collections
	public db: Db

	constructor() {}
	async connect(): Promise<void> {
		await this.mongoClient.connect()
		this.db = this.mongoClient.db(process.env.DB_NAME)
	}
}

