import { ObjectId } from 'mongodb'
import { Customer } from './customer'

export class CustomerAnonymised extends Customer {}
