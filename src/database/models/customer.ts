import { ObjectId } from 'mongodb'

export class Customer {
	_id: ObjectId
	firstName: string
	lastName: string
	email: string
	address: Address
}

 export class Address {
	line1: string
	line2: string
	postcode: string
	city: string
	state: string
	country: string
}

