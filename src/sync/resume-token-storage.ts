import * as fs from 'fs/promises';

export interface ResumeToken {
  _data: string;
}

export class ResumeTokenStorage {
  constructor(private readonly filePath: string) {}
  async save(resumeToken: string): Promise<void> {
    await fs.writeFile(this.filePath, resumeToken);
  }

  async get(): Promise<ResumeToken> {
    const token = (await fs.readFile(this.filePath)).toString();
    if (token === '') throw Error('Empty token');
    return { _data: token };
  }
}
