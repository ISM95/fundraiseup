import {
  ChangeStreamDocument,
  ChangeStreamInsertDocument,
  ChangeStreamUpdateDocument,
  Collection,
  UpdateFilter,
} from 'mongodb';
import { MongoDatabase } from '../database';
import { Customer } from '../database/models/customer';
import { CustomerAnonymised } from '../database/models/customers-anonymised';
import * as lodash from 'lodash';
import * as dotenv from 'dotenv';
dotenv.config();

import { AnonimeseCustomer } from './anonimiser';
import { FULL_REINDEX_BATCH_LIMIT } from '../constants/sync.constants';
import { ResumeToken, ResumeTokenStorage } from './resume-token-storage';
import { ChangeListener } from './change-listener';
import { TimeoutHandler } from './timer';

enum Modes {
  Sync = 'sync',
  FullReindex = 'full-reindex',
}
interface ChangedDataWithToken {
  data: Customer;
  changeStreamRecordToken: ResumeToken;
  removeFields?: string[];
}

export class App {
  private customersCollection: Collection<Customer>;
  private customersAnonymisedCollection: Collection<CustomerAnonymised>;
  private resumeTokenStorage: ResumeTokenStorage;
  private mode: Modes = Modes.Sync;
  private changeListener: ChangeListener<Customer>;
  private timeoutHandler: TimeoutHandler;
  private toChange: ChangedDataWithToken[] = [];

  constructor(
    private readonly mongoDatabase: MongoDatabase,
    private readonly anonimiseCustomer: AnonimeseCustomer,
  ) {
    this.resumeTokenStorage = new ResumeTokenStorage('resume-token');
  }

  async initialize() {
    this.initializeMode();
    await this.mongoDatabase.connect();

    this.changeListener = new ChangeListener(
      this.mongoDatabase.db.collection<Customer>('customers'),
      this.resumeTokenStorage,
    );
    this.customersAnonymisedCollection =
      this.mongoDatabase.db.collection<CustomerAnonymised>(
        'customers_anonymised',
      );
    this.customersCollection =
      this.mongoDatabase.db.collection<Customer>('customers');

    this.timeoutHandler = new TimeoutHandler(this.releaseToChangeBatch);
  }

  async start() {
    switch (this.mode) {
      case Modes.FullReindex:
        await this.executeModeFullReindex();
        process.exit(0);

      case Modes.Sync:
        this.executeModeSync();
        break;
      default:
        process.exit(0);
    }
  }

  private initializeMode() {
    process.argv.forEach((val) => {
      if (val === Modes.FullReindex) {
        this.mode = Modes.FullReindex;
      }
    });
  }

  private handleInsertEvent(
    event: ChangeStreamInsertDocument<Customer>,
    resumeToken: ResumeToken,
  ) {
    this.toChange.push({
      data: this.anonimiseCustomer.anonymiseAllFields(event.fullDocument),
      changeStreamRecordToken: resumeToken,
    });
  }

  private async handleUpdateEvent(
    event: ChangeStreamUpdateDocument<Customer>,
    resumeToken: ResumeToken,
  ) {
    const found = await this.customersAnonymisedCollection.findOne({
      _id: event.documentKey._id,
    });
    if (!found) {
      const original = await this.customersCollection.findOne({
        _id: event.documentKey._id,
      });
      this.toChange.push({
        data: this.anonimiseCustomer.anonymiseAllFields(original),
        changeStreamRecordToken: resumeToken,
      });
    } else {
      const data = this.anonimiseCustomer.anonimiseFields(
        found,
        event.updateDescription.updatedFields,
      );
      const toPush: ChangedDataWithToken = {
        data: data,
        changeStreamRecordToken: resumeToken,
      };
      if (event.updateDescription.removedFields) {
        toPush.removeFields = event.updateDescription.removedFields;
      }
      this.toChange.push(toPush);
    }
  }

  private handleChangeOnCustomersChangeStream = async (
    event: ChangeStreamDocument<Customer>,
  ) => {
    const resumeToken = event._id as ResumeToken;
    if (event.operationType === 'insert') {
      this.handleInsertEvent(event, resumeToken);
    } else if (event.operationType === 'update') {
      await this.handleUpdateEvent(event, resumeToken);
    } else {
      this.resumeTokenStorage.save(resumeToken._data);
    }
    if (this.toChange.length >= 1000) {
      this.releaseToChangeBatch();
    }
  };

  private releaseToChangeBatch = async (): Promise<void> => {
    if (this.toChange.length) {
      const session = this.mongoDatabase.mongoClient.startSession();
      try {
        session.startTransaction();
        const toChange = [...this.toChange];
        this.toChange = [];

        await this.customersAnonymisedCollection.bulkWrite(
          toChange.flatMap((e) => {
            const operations = [];

            const updateOptions: UpdateFilter<CustomerAnonymised> = {
              $set: e.data,
            };

            operations.push({
              updateOne: {
                filter: {
                  _id: e.data._id,
                },
                update: updateOptions,
                upsert: true,
              },
            });
            if (e.removeFields?.length) {
              operations.push({
                updateOne: {
                  filter: {
                    _id: e.data._id,
                  },
                  update: {
                    $unset: e.removeFields.reduce((acc, f) => {
                      return { ...acc, [f]: '' };
                    }, {}),
                  },
                  upsert: true,
                },
              });
            }
            return operations;
          }),

          { session: session },
        );
        this.resumeTokenStorage.save(
          toChange[toChange.length - 1].changeStreamRecordToken._data,
        );
        await session.commitTransaction();
      } catch (err) {
        await session.abortTransaction();
        await session.endSession();
        throw err;
      }
      await session.endSession();
    }
    this.timeoutHandler.refresh();
  };

  private executeModeSync() {
    console.log('Start execute sync mode');
    this.changeListener.listenChangesInCollection(
      this.handleChangeOnCustomersChangeStream,
    );
    this.timeoutHandler.start();
  }

  private async executeModeFullReindex() {
    console.log('Start execute with full-reindex');
    await this.customersAnonymisedCollection.deleteMany({});
    let toSkip = 0;
    let batch = await this.customersCollection
      .find()
      .limit(FULL_REINDEX_BATCH_LIMIT)
      .skip(toSkip)
      .toArray();
    toSkip += FULL_REINDEX_BATCH_LIMIT;
    await this.customersAnonymisedCollection.insertMany(
      batch.map(
        (e) => this.anonimiseCustomer.anonymiseAllFields(e) as Customer,
      ),
    );
    while (batch.length) {
      batch = await this.customersCollection
        .find()
        .limit(FULL_REINDEX_BATCH_LIMIT)
        .skip(toSkip)
        .toArray();
      toSkip += FULL_REINDEX_BATCH_LIMIT;
      if (batch.length) {
        await this.customersAnonymisedCollection.insertMany(
          batch.map(
            (e) => this.anonimiseCustomer.anonymiseAllFields(e) as Customer,
          ),
        );
      }
    }
    console.log('Executed successfuly');
  }
}