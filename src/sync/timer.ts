export class TimeoutHandler {
  private timer: NodeJS.Timeout;
  constructor(private handle: () => Promise<void>) {}

  public start() {
    this.timer = setTimeout(async () => {
      this.handle();
    }, 1000);
  }

  public refresh() {
    clearTimeout(this.timer);
    this.timer = null;
    this.start();
  }
}
