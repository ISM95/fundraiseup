import {
  ChangeStream,
  ChangeStreamDocument,
  ChangeStreamOptions,
  Collection,
} from 'mongodb';
import { ResumeTokenStorage } from './resume-token-storage';

export class ChangeListener<T> {
  private changeCollectionStream: ChangeStream<T, ChangeStreamDocument<T>>;

  constructor(
    private readonly collection: Collection<T>,
    private readonly resumeTokenStorage: ResumeTokenStorage,
  ) {}

  async listenChangesInCollection(
    handle: (event: ChangeStreamDocument<T>) => Promise<void>,
  ) {
    await this.openStream();
    this.changeCollectionStream.on('error', async (err: any) => {
      console.log(err.message);
      if (err.codeName === 'FailedToParse') {
        await this.changeCollectionStream.close();
        this.resumeTokenStorage.save('');
        await this.openStream();
        this.setOnChange(handle);
      } else {
        throw err;
      }
    });
    this.setOnChange(handle);
  }

  private async openStream() {
    const streamOptions: ChangeStreamOptions = {
      batchSize: 1000,
    };
    try {
      const startResumeToken = await this.resumeTokenStorage.get();
      streamOptions.startAfter = startResumeToken;
    } catch (err) {
      console.warn('resume-token not exists. Try to sync all customers');
    }
    this.changeCollectionStream = this.collection.watch([], streamOptions);
  }

  private setOnChange(
    handle: (event: ChangeStreamDocument<T>) => Promise<void>,
  ) {
    this.changeCollectionStream.on('change', async (event) => {
      await handle(event);
      if (event.operationType === 'invalidate') {
        await this.changeCollectionStream.close();
        await this.openStream();
        this.setOnChange(handle);
      }
    });
  }
}
