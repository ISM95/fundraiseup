import * as fs from 'fs/promises';
import { ResumeTokenStorage, ResumeToken } from './resume-token-storage';

jest.mock('fs/promises');

describe('ResumeTokenStorage', () => {
  const filePath = 'resumeToken.txt';
  let resumeTokenStorage: ResumeTokenStorage;

  beforeEach(() => {
    resumeTokenStorage = new ResumeTokenStorage(filePath);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('save', () => {
    it('should save the resume token to the specified file path', async () => {
      const resumeToken = 'abcd1234';
      const writeFileSpy = jest.spyOn(fs, 'writeFile');
      await resumeTokenStorage.save(resumeToken);
      expect(writeFileSpy).toHaveBeenCalledWith(filePath, resumeToken);
    });
  });

  describe('get', () => {
    it('should read the resume token from the specified file path and return it as a ResumeToken object', async () => {
      const resumeToken = 'abcd1234';
      const readFileSpy = jest
        .spyOn(fs, 'readFile')
        .mockResolvedValue(Buffer.from(resumeToken));
      const result = await resumeTokenStorage.get();
      expect(readFileSpy).toHaveBeenCalledWith(filePath);
      expect(result).toEqual({ _data: resumeToken });
    });

    it('should throw an error if the resume token is empty', async () => {
      const readFileSpy = jest
        .spyOn(fs, 'readFile')
        .mockResolvedValue(Buffer.from(''));
      await expect(resumeTokenStorage.get()).rejects.toThrowError(
        'Empty token',
      );
      expect(readFileSpy).toHaveBeenCalledWith(filePath);
    });
  });
});
