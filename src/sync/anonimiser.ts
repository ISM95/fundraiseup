import { Document } from 'mongodb';
import { CustomerAnonymised } from '../database/models/customers-anonymised';
import * as lodash from 'lodash';
import { Address, Customer } from '../database/models/customer';
import { generate as randomString } from 'randomstring';

const commonRandomizeAddressFields: (keyof Address)[] = [
  'line1',
  'line2',
  'postcode',
];

const commonRandomizeCustomerFields: (keyof Customer)[] = [
  'firstName',
  'lastName',
];

export class AnonimeseCustomer {
  public anonymiseAllFields(customer: Partial<Customer>): CustomerAnonymised {
    const result: Document = {};
    if (customer._id) {
      result._id = customer._id;
    }
    commonRandomizeCustomerFields.forEach((f) => {
      if (customer[f]) {
        result[f] = randomString(8);
      }
    });
    if (customer.email) {
      result.email = this.anonymiseEmail(customer.email);
    }
    if (customer.address) {
      result.address = {
        ...customer.address,
      };
      commonRandomizeAddressFields.forEach((f) => {
        if (customer.address[f]) {
          result.address[f] = randomString(8);
        }
      });
    }
    return result as CustomerAnonymised;
  }

  private prepareNestFields(fieldValueMap: Document) {
    const address: Document = {};
    if (fieldValueMap['address.line1']) {
      address.line1 = fieldValueMap['address.line1'];
    }
    if (fieldValueMap['address.line2']) {
      address.line2 = fieldValueMap['address.line2'];
    }
    if (fieldValueMap['address.postcode']) {
      address.postcode = fieldValueMap['address.postcode'];
    }
    if (fieldValueMap['address.city']) {
      address.city = fieldValueMap['address.city'];
    }
    if (fieldValueMap['address.state']) {
      address.state = fieldValueMap['address.state'];
    }
    if (fieldValueMap['address.country']) {
      address.country = fieldValueMap['address.country'];
    }
    const result: Document = {};
    if (Object.keys(address).length) {
      result.address = address;
    }
    if (fieldValueMap['firstName']) {
      result.firstName = fieldValueMap['firstName'];
    }

    if (fieldValueMap['lastName']) {
      result.lastName = fieldValueMap['lastName'];
    }
    if (fieldValueMap['email']) {
      result.email = fieldValueMap['email'];
    }
    if (fieldValueMap['address']) {
      result.address = fieldValueMap['address'];
    }
    return result;
  }
  public anonimiseFields(
    toAnonimise: CustomerAnonymised,
    fieldValueMap: Document,
  ): CustomerAnonymised {
    const preparedToAnonimiseFields = this.prepareNestFields(fieldValueMap);
    const anonymisedFields = this.anonymiseAllFields(
      preparedToAnonimiseFields as Customer,
    );
    return lodash.merge(toAnonimise, anonymisedFields);
  }

  private anonymiseEmail(email: string): string {
    const partsOfEmailField = email.split('@');
    if (partsOfEmailField.length !== 2) {
      return randomString(8);
    }
    return `${randomString(8)}@${partsOfEmailField[1]}`;
  }
}
