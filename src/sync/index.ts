import { MongoDatabase } from '../database';
import { App } from './sync';
import { AnonimeseCustomer } from './anonimiser';

const app = new App(new MongoDatabase(), new AnonimeseCustomer());
app.initialize().then(() => {
  app.start();
});
