import { TimeoutHandler } from './timer';

describe('TimeoutHandler', () => {
  let timeoutHandler: TimeoutHandler;
  let handleMock: jest.Mock;

  beforeEach(() => {
    handleMock = jest.fn().mockResolvedValue(undefined);
    timeoutHandler = new TimeoutHandler(handleMock);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should call the handle function after a specified delay', async () => {
    jest.useFakeTimers();
    timeoutHandler.start();
    expect(handleMock).not.toHaveBeenCalled();
    jest.advanceTimersByTime(1000);
    await Promise.resolve();
    expect(handleMock).toHaveBeenCalled();
  });

  it('should cancel the previous timer and start a new one when refreshed', async () => {
    jest.useFakeTimers();
    timeoutHandler.start();
    jest.advanceTimersByTime(500);
    timeoutHandler.refresh();
    jest.advanceTimersByTime(500);
    expect(handleMock).not.toHaveBeenCalled();
    jest.advanceTimersByTime(501);
    await Promise.resolve(); 
    expect(handleMock).toHaveBeenCalled();
  });
});
