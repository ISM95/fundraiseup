import { AnonimeseCustomer } from './anonimiser';

describe('AnonimeseCustomer', () => {
  let anonimeseCustomer;

  beforeEach(() => {
    anonimeseCustomer = new AnonimeseCustomer();
  });

  describe('anonymiseAllFields', () => {
    it('should anonymise all fields of a customer', () => {
      const customer = {
        _id: '123',
        firstName: 'John',
        lastName: 'Doe',
        email: 'johndoe@example.com',
        address: {
          line1: '123 Main St',
          line2: 'Apt 4B',
          postcode: '12345',
          city: 'New York',
          state: 'NY',
          country: 'USA',
        },
      };

      const anonymisedCustomer = anonimeseCustomer.anonymiseAllFields(customer);

      expect(anonymisedCustomer._id).toBe('123');
      expect(anonymisedCustomer.firstName).not.toBe('John');
      expect(anonymisedCustomer.lastName).not.toBe('Doe');
      expect(anonymisedCustomer.email).not.toBe('johndoe@example.com');
      expect(anonymisedCustomer.address.line1).not.toBe('123 Main St');
      expect(anonymisedCustomer.address.line2).not.toBe('Apt 4B');
      expect(anonymisedCustomer.address.postcode).not.toBe('12345');
      expect(anonymisedCustomer.address.city).toBe('New York');
      expect(anonymisedCustomer.address.state).toBe('NY');
      expect(anonymisedCustomer.address.country).toBe('USA');
    });
  });

  describe('anonimiseFields', () => {
    it('should anonymise specific fields of a customer', () => {
      const toAnonimise = {
        _id: '123',
        firstName: 'John',
        lastName: 'Doe',
        email: 'johndoe@example.com',
        address: {
          line1: '123 Main St',
          line2: 'Apt 4B',
          postcode: '12345',
          city: 'New York',
          state: 'NY',
          country: 'USA',
        },
      };

      const fieldValueMap = {
        _id: '123',
        firstName: 'Jane',
        lastName: 'Smith',
        email: 'janesmith@example.com',
        'address.line1': '456 Oak Ave',
      };

      const anonymisedFields = anonimeseCustomer.anonimiseFields(
        toAnonimise,
        fieldValueMap,
      );

      expect(anonymisedFields._id).toBe('123');
      expect(anonymisedFields.firstName).not.toBe('John');
      expect(anonymisedFields.lastName).not.toBe('Doe');
      expect(anonymisedFields.email).not.toBe('johndoe@example.com');
      expect(anonymisedFields.address.line1).not.toBe('123 Main St');
      expect(anonymisedFields.address.line2).toBe('Apt 4B');
      expect(anonymisedFields.address.postcode).toBe('12345');
      expect(anonymisedFields.address.city).toBe('New York');
      expect(anonymisedFields.address.state).toBe('NY');
      expect(anonymisedFields.address.country).toBe('USA');
    });
  });

  describe('prepareNestFields', () => {
    it('should prepare nested fields for anonymisation', () => {
      const fieldValueMap = {
        _id: '123',
        firstName: 'John',
        lastName: 'Doe',
        email: 'johndoe@example.com',
        'address.line1': '123 Main St',
        'address.line2': 'Apt 4B',
        'address.postcode': '12345',
        'address.city': 'New York',
        'address.state': 'NY',
        'address.country': 'USA',
      };

      const preparedFields = anonimeseCustomer.prepareNestFields(fieldValueMap);

      expect(preparedFields._id).toBe(undefined);
      expect(preparedFields.firstName).toBe('John');
      expect(preparedFields.lastName).toBe('Doe');
      expect(preparedFields.email).toBe('johndoe@example.com');
      expect(preparedFields.address.line1).toBe('123 Main St');
      expect(preparedFields.address.line2).toBe('Apt 4B');
      expect(preparedFields.address.postcode).toBe('12345');
      expect(preparedFields.address.city).toBe('New York');
      expect(preparedFields.address.state).toBe('NY');
      expect(preparedFields.address.country).toBe('USA');
    });
  });

  describe('anonymiseEmail', () => {
    it('should anonymise an email address', () => {
      const email = 'johndoe@example.com';
      const anonymisedEmail = anonimeseCustomer.anonymiseEmail(email);

      expect(anonymisedEmail).toMatch(/^[a-zA-Z0-9]{8}@example.com$/);
    });

    it('should return a random string if the email format is invalid', () => {
      const email = 'invalid_email_format';
      const anonymisedEmail = anonimeseCustomer.anonymiseEmail(email);

      expect(anonymisedEmail).toMatch(/^[a-zA-Z0-9]{8}$/);
    });
  });
});
