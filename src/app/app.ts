import { Collection } from 'mongodb';
import { MongoDatabase } from '../database';
import { Customer } from '../database/models/customer';
import { CustomerGenerator } from './customer-generator';

import * as dotenv from 'dotenv';
dotenv.config();

export class App {
  customersCollection: Collection<Customer>;
  private customerGenerator: CustomerGenerator;

  async start() {
    const mongoDatabase = new MongoDatabase();
    this.customerGenerator = new CustomerGenerator();
    await mongoDatabase.connect();

    this.customersCollection =
      mongoDatabase.db.collection<Customer>('customers');
    await this.generateAndInsertCustomers();
    setInterval(async () => {
      await this.generateAndInsertCustomers();
    }, 200);
  }

  async generateAndInsertCustomers(): Promise<void> {
    const customersToInsert = this.customerGenerator.generateMultiple();
    await this.customersCollection.insertMany(customersToInsert, {
      ordered: false,
    });
  }

  private async dropAll() {
    await this.customersCollection.deleteMany({});
  }
}


