import { ObjectId } from 'mongodb';
import { faker } from '@faker-js/faker';
import { CustomerGenerator } from './customer-generator';


jest.mock('mongodb');

describe('CustomerGenerator', () => {
    let customerGenerator: CustomerGenerator;
  
    beforeEach(() => {
      customerGenerator = new CustomerGenerator();
    });
  
    describe('generateMultiple', () => {
      it('should generate multiple customers with random data', () => {
        const customers = customerGenerator.generateMultiple();
  
        expect(Array.isArray(customers)).toBe(true);
        expect(customers.length).toBeGreaterThanOrEqual(1);
        expect(customers.length).toBeLessThanOrEqual(10);
      });
    });
  
    describe('randomOne', () => {
      it('should generate a single customer with random data', () => {
        const customer = customerGenerator.randomOne();
  
        expect(customer).toHaveProperty('_id');
        expect(customer).toHaveProperty('email');
        expect(customer).toHaveProperty('firstName');
        expect(customer).toHaveProperty('lastName');
        expect(customer).toHaveProperty('address');
        expect(customer.address).toHaveProperty('city');
        expect(customer.address).toHaveProperty('line1');
        expect(customer.address).toHaveProperty('line2');
        expect(customer.address).toHaveProperty('country');
        expect(customer.address).toHaveProperty('postcode');
        expect(customer.address).toHaveProperty('state');
      });
  
      it('should generate a unique _id for each customer', () => {
        const customer1 = customerGenerator.randomOne();
        const customer2 = customerGenerator.randomOne();
  
        expect(customer1._id).not.toBe(customer2._id);
      });
    });
  });