import { ObjectId } from 'mongodb';
import { Customer } from '../database/models/customer';
import { faker } from '@faker-js/faker';

export class CustomerGenerator {
  public generateMultiple(): Customer[] {
    const customersLength = Math.floor(Math.random() * (10 - 1 + 1) + 1);
    const customers: Customer[] = [];
    for (let i = 0; i < customersLength; i++) {
      customers.push(this.randomOne());
    }
    return customers;
  }

  public randomOne(): Customer {
    return {
      _id: new ObjectId(),
      email: faker.internet.email(),
      firstName: faker.person.firstName(),
      lastName: faker.person.lastName(),
      address: {
        city: faker.location.city(),
        line1: faker.location.streetAddress(),
        line2: faker.location.secondaryAddress(),
        country: faker.location.country(),
        postcode: faker.location.zipCode(),
        state: faker.location.state(),
      },
    };
  }
}
