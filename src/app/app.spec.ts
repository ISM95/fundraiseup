import { Collection, ObjectId } from 'mongodb';
import { Customer } from '../database/models/customer';
import { CustomerGenerator } from './customer-generator';
import { App } from './app';
import { MongoDatabase } from '../database';

jest.mock('../database');
jest.mock('./customer-generator');

describe('App', () => {
  let app: App;
  let customersCollection: Collection<Customer>;
  let customerGenerator: CustomerGenerator;

  beforeEach(() => {
    customersCollection = {
      insertMany: jest.fn(),
      deleteMany: jest.fn(),
    } as unknown as Collection<Customer>;

    customerGenerator = {
      generateMultiple: jest.fn(),
    } as unknown as CustomerGenerator;

    (MongoDatabase as jest.Mock).mockImplementation(() => ({
      connect: jest.fn(),
      db: {
        collection: jest.fn().mockReturnValue(customersCollection),
      },
    }));

    (CustomerGenerator as jest.Mock).mockImplementation(
      () => customerGenerator,
    );

    app = new App();
    app.customersCollection = customersCollection;
    app['customerGenerator'] = customerGenerator;
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('generateAndInsertCustomers', () => {
    it('should generate multiple customers and insert them into the collection', async () => {
      const id1 = new ObjectId();
      const id2 = new ObjectId();
      const customersToInsert: Customer[] = [
        {
          _id: id1,
          firstName: 'John',
          lastName: 'Doe',
          email: 'Doe@gmail.com',
          address: {
            city: '',
            country: '',
            line1: '',
            line2: '',
            postcode: '',
            state: '',
          },
        },
        {
          _id: id2,
          firstName: 'Jane',
          lastName: 'Smith',
          email: 'Doe@gmail.com',
          address: {
            city: '',
            country: '',
            line1: '',
            line2: '',
            postcode: '',
            state: '',
          },
        },
      ];

      customerGenerator.generateMultiple['mockReturnValueOnce'](
        customersToInsert,
      );

      await app.generateAndInsertCustomers();

      expect(customerGenerator.generateMultiple).toHaveBeenCalledTimes(1);
      expect(customersCollection.insertMany).toHaveBeenCalledTimes(1);
      expect(customersCollection.insertMany).toHaveBeenCalledWith(
        customersToInsert,
        { ordered: false },
      );
    });
  });
});
